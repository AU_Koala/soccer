//
//  MyAccountViewController.swift
//  Soccer
//
//  Created by MacBookPro-PC on 2017/11/07.
//  Copyright © 2017年 Cui Wei. All rights reserved.
//

import UIKit
import DeviceKit

class SettingViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout, UICollectionViewDataSource {

    @IBOutlet weak var collectionView: UICollectionView!
    private let dataSource = [1, 2, 3, 4, 5, 6]
    
    fileprivate let sectionInsets = UIEdgeInsets(top: 50.0, left: 20.0, bottom: 50.0, right: 20.0)
    fileprivate let itemsPerRow: CGFloat = 3
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        self.collectionView.delegate = self
        self.collectionView.dataSource = self
        
        self.collectionView.register(UINib(nibName: "LoginCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "LoginCollectionViewCell")
        self.collectionView.register(UINib(nibName: "ResetPasswordCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "ResetPasswordCollectionViewCell")
        self.collectionView.register(UINib(nibName: "AboutCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "AboutCollectionViewCell")
        self.collectionView.register(UINib(nibName: "LineCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "LineCollectionViewCell")
        self.collectionView.register(UINib(nibName: "FacebookCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "FacebookCollectionViewCell")
        self.collectionView.register(UINib(nibName: "TwitterCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "TwitterCollectionViewCell")
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    // UICollectionViewDelegate
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return dataSource.count
    }
    
    // UICollectionViewDelegateFlowLayout
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {

        let paddingSpace = sectionInsets.left * (itemsPerRow + 1)
        let availableWidth = view.frame.width - paddingSpace
        let widthPerItem = availableWidth / itemsPerRow

        return CGSize(width: widthPerItem, height: widthPerItem)
    }

    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        insetForSectionAt section: Int) -> UIEdgeInsets {
        return sectionInsets
    }

    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return sectionInsets.left
    }
    
    // UICollectionViewDataSource
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if dataSource[indexPath.item] == 1 {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "LoginCollectionViewCell", for: indexPath) as! LoginCollectionViewCell
            return cell
        } else if dataSource[indexPath.item] == 2 {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ResetPasswordCollectionViewCell", for: indexPath) as! ResetPasswordCollectionViewCell
            return cell
        } else if dataSource[indexPath.item] == 3 {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "AboutCollectionViewCell", for: indexPath) as! AboutCollectionViewCell
            return cell
        } else if dataSource[indexPath.item] == 4 {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "LineCollectionViewCell", for: indexPath) as! LineCollectionViewCell
            return cell
        } else if dataSource[indexPath.item] == 5 {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "FacebookCollectionViewCell", for: indexPath) as! FacebookCollectionViewCell
            return cell
        } else {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "TwitterCollectionViewCell", for: indexPath) as! TwitterCollectionViewCell
            return cell
        }
    }
}
